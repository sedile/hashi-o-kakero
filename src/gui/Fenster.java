package gui;

import hashi.Insel;
import hashi.Bild;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.WritableImage;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import dateimanager.Laden;
import dateimanager.Speichern;
import verfahren.Generator;
import verfahren.Loesung;
import verfahren.SichereBruecke;
import verfahren.Simulation;

public class Fenster extends Stage {

	private BorderPane layout;
	private Bild spielfeld;
	private Simulation sim;
	private Insel[][] inseln, inselnloesung;
	private ArrayList<Line> linienliste;
	private Label status;
	private final String[] aktStatus = {"Raetsel ist geloest","Raetsel ist noch nicht geloest","Raetsel enthaelt einen Fehler"};
	private MenuBar jmenubar;
	private Menu raetsel, sonstiges;
	private MenuItem neu, start, beenden, speichern, laden, gSpeichern;
	private CheckBox zeigeRestVerbindungen;
	private Button simulation;
	private Button sichereKante;
	private Button loesung;
	
	private int breite, hoehe;
	private boolean aktiviert = false;
	
	public Fenster(){
		initialisieren();
	}

	private void initialisieren(){
		breite = 10;
		hoehe = 10;
		inseln = new Insel[breite][hoehe];
		inselnloesung = new Insel[breite][hoehe];
		linienliste = new ArrayList<Line>();
		panelInit();
		komponentenInit();

		Scene inhalt = new Scene(layout);
		setScene(inhalt);
		setMinWidth(640);
		setMinHeight(480);
		setTitle("Hashi-o-Kakero");
		setResizable(false);
		show();
	}
	
	public void setDaten(Insel[][] inseln, Insel[][] loesung, ArrayList<Line> linien, int breite, int hoehe){
		this.inseln = inseln;
		inselnloesung = loesung;
		linienliste = linien;
		this.breite = breite;
		this.hoehe = hoehe;
		spielfeld.updateCanvasGroesse();
		spielfeld.zeichnen();
	}
	
	private void panelInit(){
		layout = new BorderPane();

        Generator g = new Generator(breite,hoehe);
        inseln = g.getInseln();
        inselnloesung = g.getLoesung();
		spielfeld = new Bild(this);
		ScrollPane scroll = new ScrollPane(spielfeld);
		layout.setCenter(scroll);
	}
	
	private void komponentenInit(){
		jmenubar = new MenuBar();
		raetsel = new Menu("Raetsel");
		sonstiges = new Menu("Spielstand");
		
		neu = new MenuItem("Neues Raetsel");
		neu.setOnAction(new MenuHandler());
		start = new MenuItem("Raetsel neu versuchen");
		start.setOnAction(new MenuHandler());	
		beenden = new MenuItem("Beenden");
		beenden.setOnAction(new MenuHandler());
		speichern = new MenuItem("Spielstand speichern");
		speichern.setOnAction(new MenuHandler());		
		laden = new MenuItem("Spielstand laden");
		laden.setOnAction(new MenuHandler());
		gSpeichern = new MenuItem("Grafik speichern");
		gSpeichern.setDisable(false);
		gSpeichern.setOnAction(new MenuHandler());
		
		sonstiges.getItems().addAll(speichern,laden,gSpeichern);
		raetsel.getItems().addAll(neu,start,beenden);
		jmenubar.getMenus().addAll(raetsel,sonstiges);
		layout.setTop(jmenubar);

		BorderPane b = new BorderPane();
		status = new Label(aktStatus[1]);
		simulation = new Button("Simulation starten");
		simulation.setDisable(false);
		simulation.setOnAction(new MenuHandler());
		
		sichereKante = new Button("Bruecke einzeichnen (Hilfe)");
		sichereKante.setOnAction(new MenuHandler());
		
		loesung = new Button("Loesung anzeigen");
		loesung.setOnAction(new MenuHandler());
		
		zeigeRestVerbindungen = new CheckBox();
		zeigeRestVerbindungen.setText("Zeige die restlichen Verbindungen an");
		zeigeRestVerbindungen.setOnAction(new MenuHandler());
		
		b.setTop(zeigeRestVerbindungen);
		b.setLeft(simulation);
		b.setCenter(sichereKante);
		b.setRight(loesung);
		b.setBottom(status);
		layout.setBottom(b);
	}
	
	/**
	 * Erzeugt ein neues Raetsel
	 */
	private void neuesRaetsel(){
		NeuesRaetsel nr = new NeuesRaetsel(this);
		
		if ( nr.getRaetselgroesse()[0] < 4 || nr.getRaetselgroesse()[1] < 4){
			return;
		}
		
		breite = nr.getRaetselgroesse()[0];
		hoehe = nr.getRaetselgroesse()[1];
		Generator g = new Generator(breite,hoehe);
		inseln = g.getInseln();
		inselnloesung = g.getLoesung();
		linienliste.clear();
		loesung.setDisable(false);
		start.setDisable(false);
		simulation.setDisable(false);
		sichereKante.setDisable(false);
		
		getLinien().clear();
		spielfeld.updateCanvasGroesse();
		spielfeld.zeichnen();
		setStatustext(1);
	}
	
	/**
	 * Setzt das Raetsel vollstaendig zurueck
	 * 
	 * - Alle ausgehenden Verbindungen der Inseln werden auf 0 gesetzt
	 * - Alle Linien werden aus der Liste geloescht
	 * - Das Spielfeld wird neugezeichnet
	 * - Der Status wird auf "noch nicht geloest" gesetzt
	 */
	public void neustarten(){
		try {
			for(int i = 0; i < getInseln().length; i++){
				for(int j = 0; j < getInseln()[i].length; j++){
					if(getInseln()[i][j] != null ){
						
						/* Alle Inseldaten zuruecksetzen */
						getInseln()[i][j].anzahlVerbindungNord = 0;
						getInseln()[i][j].anzahlVerbindungOst = 0;
						getInseln()[i][j].anzahlVerbindungSued = 0;
						getInseln()[i][j].anzahlVerbindungWest = 0;
						getInseln()[i][j].aktuelleVerbindungen = 0;
						getInseln()[i][j].restVerbindungen = getInseln()[i][j].maxAnzahlVerbindungen;
						
						/* Alle eingezeichneten Linien in der Liste entfernen und
						 * Status zurueckversetzen */
						getLinien().clear();
						spielfeld.zeichnen();
						setStatustext(1);
					}
				}
			}
			loesung.setDisable(false);
			sichereKante.setDisable(false);
			setGrafikItem(true);
			
		} catch ( NullPointerException e){
			Alert f = new Alert(AlertType.WARNING);
			f.setHeaderText("Fehler");
			f.setContentText("Fehler beim zuruecksetzten des Raetsels (Inselfehler)");
			f.showAndWait();
		}
	}
	
	/**
	 * speichert ein Spielstand in eine HRD-Datei ab
	 */
	private void speichern(){
		try {
			new Speichern(this);
		} catch ( NullPointerException e){
			Alert f = new Alert(AlertType.ERROR);
			f.setHeaderText("Speichern fehlgeschlagen");
			f.setContentText("Fehler beim speichern des Spielstandes (Indexfehler)");
			f.showAndWait();
		}
	}
	
	/**
	 * laedt ein Spielstand
	 */
	private void laden(){
		try {
			new Laden(this);
			status.setText(aktStatus[1]);
			sichereKante.setDisable(false);
		} catch (NumberFormatException num){
			Alert f = new Alert(AlertType.ERROR);
			f.setHeaderText("Laden fehlgeschlagen");
			f.setContentText("Spielstand enthaelt ungueltige Zeichen (Syntaxfehler)");
			f.showAndWait();
		}
	}

	public void setGrafikItem(boolean status){
		gSpeichern.setDisable(false);
	}

	/**
	 * speichert ein generiertes ungeloestes Raetsel als png-Grafik
	 * 
	 * @param datei abzuspeichernde Datei
	 * @throws IOException
	 */
	private void speicherGrafik(){
		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("PNG-Datei", "*.png"));
		File bild = fc.showSaveDialog(this);
		
		if ( bild != null ){
			try {
				WritableImage img = new WritableImage((int) spielfeld.getWidth(), (int) spielfeld.getHeight());
				spielfeld.snapshot(null, img);
				RenderedImage rimg = SwingFXUtils.fromFXImage(img, null);
				ImageIO.write(rimg, "png", bild);
				Alert f = new Alert(AlertType.INFORMATION);
				f.setHeaderText("PNG-Grafik gespeichert");
				f.setContentText("Grafik wurde gespeichert");
				f.showAndWait();
			} catch ( IOException ioe ){
				Alert f = new Alert(AlertType.WARNING);
				f.setHeaderText("Fehler");
				f.setContentText("Grafik konnte nicht gespeichert werden");
				f.showAndWait();
			}
		}
	}

	/**
	 * Zeigt die Loesung des Raetsels an
	 */
	public void zeigeLoesung(){
		try {
			neustarten();
			sichereKante.setDisable(true);
			new Loesung(this);
		} catch ( NullPointerException e){
			Alert f = new Alert(AlertType.WARNING);
			f.setHeaderText("Fehler");
			f.setContentText("Fehler beim ermitteln der Loesung (Inselfehler)");
			f.showAndWait();
		}
	}
	
	/** versucht eine konfliktfreie Bruecke einzuzeichnen */
	public void sichereKante(){
		new SichereBruecke(this);
	}
	
	/**
	 *  Simuliert eine Loesung des Raetsels indem es mithilfe
	 *  eines Threads versucht das Raetsel automatisch zu
	 *  Loesen
	 */
	public void simulation(){
		if (inseln == null){
			Alert f = new Alert(AlertType.WARNING);
			f.setHeaderText("Fehler");
			f.setContentText("Raetsel kann nicht geloest werden (Threadfehler)");
			f.showAndWait();
		} else {			
			try {
				neustarten();
				sichereKante.setDisable(true);
				sim = new Simulation(this);
				sim.starten();			
			} catch ( Exception e){
				sim.stoppen();
				Alert f = new Alert(AlertType.WARNING);
				f.setHeaderText("Fehler");
				f.setContentText("Fehler beim automatischen Loesen des Raetsels (Threadfehler)");
				f.showAndWait();
			}
		}
	}
	
	public void setStatustext(int index){
		status.setText(aktStatus[index]);
	}
	
	public String[] getStatusfeld(){
		return aktStatus;
	}
	
	public String getStatustext(){
		return status.getText();
	}
	
	public void aktiviereSimulationButton(boolean aktivieren){
		simulation.setDisable(aktivieren);
	}
	
	public Insel[][] getInseln(){
		return inseln;
	}
	
	public Insel[][] getLoesung(){
		return inselnloesung;
	}
	
	public void addLinie(Line linie){
		linienliste.add(linie);
	}
	
	public void entferneLinie(Line linie){
		linienliste.remove(linie);
	}
	
	public ArrayList<Line> getLinien(){
		return linienliste;
	}
	
	public int getBreite(){
		return breite;
	}
	
	public int getHoehe(){
		return hoehe;
	}
	
	public Bild getSpielfeld(){
		return spielfeld;
	}
	
	private class MenuHandler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent menu) {
			if ( menu.getSource() == neu ){
				neuesRaetsel();
			} else if ( menu.getSource() == start ){
				neustarten();
			} else if ( menu.getSource() == speichern ){
				speichern();
			} else if ( menu.getSource() == laden ){
				laden();
			} else if ( menu.getSource() == gSpeichern ){
				speicherGrafik();
			} else if ( menu.getSource() == beenden ){
				System.exit(0);
			} else if ( menu.getSource() == loesung){
				zeigeLoesung();
			} else if ( menu.getSource() == sichereKante){
				sichereKante();
			} else if ( menu.getSource() == simulation){
				simulation();
			} else if ( menu.getSource() == zeigeRestVerbindungen && aktiviert == true){
				spielfeld.anzeigeMaxOderRestVerbindungen(aktiviert);
				aktiviert = false;
			} else if ( menu.getSource() == zeigeRestVerbindungen && aktiviert == false){
				spielfeld.anzeigeMaxOderRestVerbindungen(aktiviert);
				aktiviert = true;
			}
		}
	} // MenuHandler ende

}
