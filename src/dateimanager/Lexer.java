package dateimanager;

import java.util.StringTokenizer;

/**
 * Diese Klasse prueft ein eingelesenen Text auf erwartete Symbole die an den
 * Parser weitergegeben werden
 * 
 * @author Sebastian
 *
 */
public class Lexer {

	private int anzahlTokens = 0;
    private StringTokenizer stringTokenizer;
    private String aktuellesSymbol;
   
    /** Diese Enumaeration enthaelt alle Symbole die Gemaess der EBNF-Grammatik gueltig sind */
    enum Symbol{
        FELD, GENINSEL, AKTINSEL, LINIE, ENDE, LKLAMMER_ECKIG, RKLAMMER_ECKIG, LKLAMMER_RUND, RKLAMMER_RUND, KOMMA, ZAHL;
    }
   
    /**
     * Der Konstruktor enthaelt ein String der den eingelesenen Text speichert
     * 
     * @param text	Der uebergebene eingelesene Text als String
     */
    public Lexer(String text) {
       
    	String eText = text;
    	
    	eText = eText.replace(","," ,");
        stringTokenizer = new StringTokenizer(eText);
    }
   
    /**
     * Prueft den aktuellen Token, um welches Symbol es sich handelt
     * @return Symbol
     */
    Symbol getNaechstesSymbol(){
       
        if(stringTokenizer.hasMoreTokens()){
            anzahlTokens++;
            aktuellesSymbol = stringTokenizer.nextToken();
           
            if(aktuellesSymbol.matches("#FELD")){
                return Symbol.FELD;
            }
           
            switch(aktuellesSymbol){
           
            case "#FELD":		return Symbol.FELD;
            case "#GENINSEL":	return Symbol.GENINSEL;
            case "#AKTINSEL":	return Symbol.AKTINSEL;
            case "#LINIE":		return Symbol.LINIE;
            case "<":			return Symbol.LKLAMMER_ECKIG;
            case "(":			return Symbol.LKLAMMER_RUND;
            case ">":			return Symbol.RKLAMMER_ECKIG;
            case ")":			return Symbol.RKLAMMER_RUND;
            case ",":			return Symbol.KOMMA;
            case "[0-9]*":		return Symbol.ZAHL;           
            
            }
        }
        return Symbol.ENDE;
    }

    /**
     * Gibt die Menge der Tokens als Integer zurueck
     * 
     * @return	Die Anzahl der Tokens
     */
    public int getAnzahlTokensAusString(){
    	return anzahlTokens;
    }
}
