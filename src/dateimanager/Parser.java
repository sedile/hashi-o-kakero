package dateimanager;

import dateimanager.Lexer.Symbol;

/**
 * Der Parser ist fuer die Ueberpruefung zustaendig, ob die Eingelesene
 * SUR-Datei auch Syntaktisch korrekt ist. Bei einem Lesefehler wird das
 * Einlesen mit einer Fehlermeldung abgebrochen.
 * 
 * @author Sebastian
 *
 */
	public class Parser {
		
		private int tokens = 0;
	    private Lexer lexer;
	    private Symbol aktSymbol;

	    /**
	     * Der Parserkonstruktor enthaelt ein Objekt vom Typ Lexer als Parameter
	     * 
	     * @param lexer		Ein Lexer-Objekt
	     */
	    public Parser(Lexer lexer) {
	        this.lexer = lexer;
	    }
	   
	    /** Diese Methode beginnt mit dem Parsen des eingelesenen Textes, indem ueberprueft
	     *  wird, ob ein eingelesenes Symbol an einer Stelle gueltig ist oder nicht
	     * 
	     * @return true, falls kein Syntaxfehler vorliegt
	     */
	    boolean parseText(){
	       
	    	/* Wenn ein Syntaxfehler auftritt, sofortiger abbruch, da Datei ungÃ¼ltig */
	        if(!outputFile()){
	            return false;
	        }
            return true;
	    }

	    /**
	     * Wurzel des Methodenbaums
	     * @return
	     */
	    private boolean outputFile(){	    	
	    	aktSymbol = lexer.getNaechstesSymbol();
	    	tokens++;
	    	
	    	if(!feld()){
	            return false;
	    	}
	    	
	    	if(!genInsel()){
	            return false;
	    	}
	    	
	    	if(!aktInsel()){
	            return false;
	    	}
	    	
	    	if(!linie()){
	            return false;
	    	}

            return true;
	    }
	    
	    private boolean feld(){	    	
	    	/* Das Wort '#FELD' */
	    	if ( aktSymbol == Symbol.FELD){
	    		aktSymbol = lexer.getNaechstesSymbol();
	    		tokens++;
	    		
	    		/* Felddaten */
	    		if (!feldDaten()){
	    			return false;
	    		}
	    	} else {
	    		return false;
	    	}
	    	
	    	return true;
	    }
	    
	    private boolean genInsel(){	    	
	    	/* Das Wort '#GENINSEL' */
	    	if ( aktSymbol == Symbol.GENINSEL){
	    		aktSymbol = lexer.getNaechstesSymbol();
	    		tokens++;
	    		
	    		/* Inseldaten */
	    		if (!genInseln()){
	    			return false;
	    		}
	    	} else {
	    		return false;
	    	}
	    	return true;
	    }
	    
	    private boolean aktInsel(){	    	
	    	/* Das Wort '#AKTINSEL' */
	    	if ( aktSymbol == Symbol.AKTINSEL){
	    		aktSymbol = lexer.getNaechstesSymbol();
	    		tokens++;
	    		
	    		while ( aktSymbol == Symbol.LKLAMMER_ECKIG){
		    		if (!genInseln()){
		    			return false;
		    		}
	    		}
	    	} else {
	    		return false;
	    	}
	    	return true;
	    }
	    
	    private boolean linie(){	    	
	    	/* Das Wort '#LINIE' */
	    	if ( aktSymbol == Symbol.LINIE){
	    		aktSymbol = lexer.getNaechstesSymbol();
	    		tokens++;
	    		
	    		while ( aktSymbol == Symbol.LKLAMMER_RUND){
		    		if (!linien()){
		    			return false;
		    		}
	    		}
	    	} else {
	    		return false;
	    	} 	
	    	return true;
	    }

	    private boolean feldDaten(){	    	
	    	if ( aktSymbol == Symbol.LKLAMMER_ECKIG){
	    		aktSymbol = lexer.getNaechstesSymbol();
	    		tokens++;
	    		
	    		if (!ziffer()){
	    			return false;
	    		}
	    		
    			if ( aktSymbol == Symbol.KOMMA){
    				aktSymbol = lexer.getNaechstesSymbol();
    				tokens++;
    				
    	    		if (!ziffer()){
    	    			return false;
    	    		}
    	    		
    				if ( aktSymbol == Symbol.KOMMA){
    					aktSymbol = lexer.getNaechstesSymbol();
    					tokens++;
    					
		    			if (!ziffer()){
		    				return false;
		    			}
		    			
    					if ( aktSymbol == Symbol.RKLAMMER_ECKIG){
    						aktSymbol = lexer.getNaechstesSymbol();
    						tokens++;	
    					} else {
    						return false;
    					}
    				} else {
        	    		return false;
        	    	}
    			} else {
    	    		return false;
    	    	}	
	    	} else {
	    		return false;
	    	}
	    	return true;
	    }
	    
	    private boolean genInseln(){    	
	    	if ( aktSymbol == Symbol.LKLAMMER_ECKIG){
	    		aktSymbol = lexer.getNaechstesSymbol();
	    		tokens++;
	    		
	    		if (!ziffer()){
	    			return false;
	    		}
	    		
    			if ( aktSymbol == Symbol.KOMMA){
    				aktSymbol = lexer.getNaechstesSymbol();
    				tokens++;
    				
    	    		if (!ziffer()){
    	    			return false;
    	    		}
    	    		
        			if ( aktSymbol == Symbol.KOMMA){
        				aktSymbol = lexer.getNaechstesSymbol();
        				tokens++;
        				
        	    		if (!ziffer()){
        	    			return false;
        	    		}
        	    		
            			if ( aktSymbol == Symbol.KOMMA){
            				aktSymbol = lexer.getNaechstesSymbol();
            				tokens++;
            				
            	    		if (!ziffer()){
            	    			return false;
            	    		}
            	    		
                			if ( aktSymbol == Symbol.KOMMA){
                				aktSymbol = lexer.getNaechstesSymbol();
                				tokens++;
                				
                   	    		if (!ziffer()){
                	    			return false;
                	    		}
                				
                    			if ( aktSymbol == Symbol.KOMMA){
                    				aktSymbol = lexer.getNaechstesSymbol();
                    				tokens++;
                    				
                       	    		if (!ziffer()){
                    	    			return false;
                    	    		}
                       	    		
                        			if ( aktSymbol == Symbol.KOMMA){
                        				aktSymbol = lexer.getNaechstesSymbol();
                        				tokens++;
                        				
                           	    		if (!ziffer()){
                        	    			return false;
                        	    		}
                        				
                               			if ( aktSymbol == Symbol.RKLAMMER_ECKIG){
                            				aktSymbol = lexer.getNaechstesSymbol();
                            				tokens++;
                            			} else {
                            				return false;
                            			}
                        			} else {
                        				return false;
                        			}                 				
                    			} else {
                    				return false;
                    			}                   	    		
                			} else {
                				return false;
                			}            				
            			} else {
            				return false;
            			}       				
        			} else {
        				return false;
        			}		
    			} else {
    				return false;
    			}	    		
	    	} else {
	    		return false;
	    	}	
	    	return true;
	    }
	    
	    private boolean linien(){	    	
	    	if ( aktSymbol == Symbol.LKLAMMER_RUND){
	    		aktSymbol = lexer.getNaechstesSymbol();
	    		tokens++;
	    		
  	    		if (!ziffer()){
	    			return false;
	    		}
  	    		
  		    	if ( aktSymbol == Symbol.KOMMA){
  		    		aktSymbol = lexer.getNaechstesSymbol();
  		    		tokens++;
  		    		
  	  	    		if (!ziffer()){
  		    			return false;
  		    		}
  	  	    		
  	  		    	if ( aktSymbol == Symbol.KOMMA){
  	  		    		aktSymbol = lexer.getNaechstesSymbol();
  	  		    		tokens++;
  	  		    		
  	  	  	    		if (!ziffer()){
  	  		    			return false;
  	  		    		}
  	  	  	    		
  	  	  		    	if ( aktSymbol == Symbol.KOMMA){
  	  	  		    		aktSymbol = lexer.getNaechstesSymbol();
  	  	  		    		tokens++;
  	  	  		    		
  	  	  	  	    		if (!ziffer()){
  	  	  		    			return false;
  	  	  		    		}
  	  	  	  	    		
  	  	  	  		    	if ( aktSymbol == Symbol.RKLAMMER_RUND){
  	  	  	  		    		aktSymbol = lexer.getNaechstesSymbol();
  	  	  	  		    		tokens++;
  	  	  	  		    	} else {
  	  	  	  		    		return false;
  	  	  	  		    	}  	  		    		
  	  	  		    	} else {
  	  	  		    		return false;
  	  	  		    	}
  	  		    	} else {
  	  		    		return false;
  	  		    	}		    		
  		    	} else {
  		    		return false;
  		    	}	    		
	    	} else {
	    		return false;
	    	}
	    	return true;
	    }
	    
	    private boolean ziffer(){	    	
	    	if ( aktSymbol == Symbol.ZAHL){
	    		aktSymbol = lexer.getNaechstesSymbol();
	    		tokens++;
	            return true;
	    	}	    	
            return false;
	    }
	    
	    public int getAnzahlTokensAusParser(){
	    	return tokens;
	    }
}
