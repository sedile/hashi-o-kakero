package dateimanager;

import java.io.File;
import java.io.FileWriter;

import gui.Fenster;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class Speichern {
	
	private Fenster fenster;
	private int breite;
	private int hoehe;
	private int anzahlInseln = 0;
	private String daten = "";
	
	/**
	 * Verzeichnis der letzten Datei
	 */
	static String letzteDatei;
	
	private FileChooser fc;

	public Speichern(Fenster f) throws NullPointerException {
		fenster = f;	
		breite = fenster.getBreite();
		hoehe = fenster.getHoehe();

		fc = new FileChooser();
		fc.setTitle("Spielstand speichern...");
		fc.getExtensionFilters().addAll(new ExtensionFilter("HRD-Datei", "*.hrd"));
		datensammeln();
	}
	
	private void datensammeln(){
		for(int x = 0; x < fenster.getInseln().length; x++){
			for(int y = 0; y < fenster.getInseln()[x].length; y++){
				if( fenster.getInseln()[x][y] != null ){
					anzahlInseln++;
				}
			}
		}

		String feld = "#FELD\r\n<"+breite+","+hoehe+","+anzahlInseln+">\r\n";
		daten += feld+"\r\n";
		
		String genInseln = "#GENINSEL\r\n";
		for(int x = 0; x < fenster.getLoesung().length; x++){
			for(int y = 0; y < fenster.getLoesung()[x].length; y++){
				if(fenster.getLoesung()[x][y] != null){
					genInseln += "<"+x+","+y+","+fenster.getLoesung()[x][y].getVerbindungNord()+","+fenster.getLoesung()[x][y].getVerbindungOst()+","+fenster.getLoesung()[x][y].getVerbindungSued()+","+fenster.getLoesung()[x][y].getVerbindungWest()+","+fenster.getLoesung()[x][y].getMaxAnzahlVerbindung()+">\r\n";
				}
			}
		}
		daten += genInseln+"\r\n";
		
		String aktInseln = "#AKTINSEL\r\n";
		for(int x = 0; x < fenster.getInseln().length; x++){
			for(int y = 0; y < fenster.getInseln()[x].length; y++){
				if(fenster.getInseln()[x][y] != null){
					aktInseln += "<"+x+","+y+","+fenster.getInseln()[x][y].getVerbindungNord()+","+fenster.getInseln()[x][y].getVerbindungOst()+","+fenster.getInseln()[x][y].getVerbindungSued()+","+fenster.getInseln()[x][y].getVerbindungWest()+","+fenster.getInseln()[x][y].getMaxAnzahlVerbindung()+">\r\n";
				}
			}
		}
		daten += aktInseln+"\r\n";
		
		String linien = "#LINIE\r\n";
		for(int i = 0; i < fenster.getLinien().size(); i++){
			linien += "("+(int) fenster.getLinien().get(i).getStartX()+","+(int) fenster.getLinien().get(i).getStartY()+","+(int) fenster.getLinien().get(i).getEndX()+","+(int) fenster.getLinien().get(i).getEndY()+")\r\n";
		}
		daten += linien+"\r\n";
		
		speichern();
	}
	
	private void speichern(){
		File speicherdatei = fc.showSaveDialog(fenster);
		
		if ( speicherdatei != null){
			letzteDatei = speicherdatei.getAbsolutePath();
			try {
				FileWriter schreiber = new FileWriter(speicherdatei.getPath());
				schreiber.write(daten);
				schreiber.flush();
				schreiber.close();
				
				Alert f = new Alert(AlertType.INFORMATION);
				f.setHeaderText("Speichern erfolgreich");
				f.setContentText("Spielstand wurde erfolgreich gespeichert");
				f.showAndWait();			
			} catch (Exception e){
				Alert f = new Alert(AlertType.ERROR);
				f.setHeaderText("Speichern fehlgeschlagen");
				f.setContentText("Fehler beim speichern des Spielstandes");
				f.showAndWait();
			}
		}
	}

}
