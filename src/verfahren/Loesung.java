package verfahren;

import gui.Fenster;

public class Loesung {
	
	private Fenster fenster;
	private int breite;
	private int hoehe;

	public Loesung(Fenster f){
		fenster = f;
		breite = fenster.getBreite();
		hoehe = fenster.getHoehe();
		ergaenzeSichereBruecke();
	}

	/** ueberprueft ausgehend von einer existierenden Insel, ob von dort aus eine
	 *  sichere Bruecke zu seinen Nachbarn (Nord, Ost, Sued, West) ergÃ¤nzt werden kann
	 *  
	 *  @return Koordinate der Insel, wo eine Bruecke ausgeht
	 */
	private void ergaenzeSichereBruecke(){
		Nachbarn n = new Nachbarn(fenster);
		
		for (int i = 0; i < breite; i++){
			for( int j = 0; j < hoehe; j++){
				if ( fenster.getInseln()[i][j] != null && fenster.getLoesung()[i][j] != null ){
					
					// ueberpruefe die ausgehenden Verbindungen nach Norden
					if ( fenster.getInseln()[i][j].anzahlVerbindungNord < fenster.getLoesung()[i][j].anzahlVerbindungNord){

						if ( n.getNordNachbar(i,j) < j && n.getNordNachbar(i,j) != j){
							fenster.getSpielfeld().verbindungHinzufuegen(fenster.getInseln()[i][j],fenster.getInseln()[i][n.getNordNachbar(i,j)]);
						}
						// ueberpruefe die ausgehenden Verbindungen nach Osten
					}
					
					if ( fenster.getInseln()[i][j].anzahlVerbindungOst < fenster.getLoesung()[i][j].anzahlVerbindungOst){
						
						if ( n.getOstNachbar(i,j) > i && n.getOstNachbar(i,j) != i) {
							fenster.getSpielfeld().verbindungHinzufuegen(fenster.getInseln()[i][j], fenster.getInseln()[n.getOstNachbar(i,j)][j]);
						}
						// ueberpruefe die ausgehenden Verbindungen nach Sueden
					}
					
					if ( fenster.getInseln()[i][j].anzahlVerbindungSued < fenster.getLoesung()[i][j].anzahlVerbindungSued){
						
						if ( n.getSuedNachbar(i,j) > j && n.getSuedNachbar(i,j) != j) {
							fenster.getSpielfeld().verbindungHinzufuegen(fenster.getInseln()[i][j],fenster.getInseln()[i][n.getSuedNachbar(i,j)]);
						}
						// ueberpruefe die ausgehenden Verbindungen nach Westen
					}

					if ( fenster.getInseln()[i][j].anzahlVerbindungWest < fenster.getLoesung()[i][j].anzahlVerbindungWest){
						
						if ( n.getWestNachbar(i,j) < i && n.getWestNachbar(i,j) != i) {
							fenster.getSpielfeld().verbindungHinzufuegen(fenster.getInseln()[i][j], fenster.getInseln()[n.getWestNachbar(i,j)][j]);
						}
					}
				}
			}
		}
	} // ergaenzeSichereBruecke ende
}
