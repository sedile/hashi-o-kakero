package verfahren;

import java.util.ArrayList;

import gui.Fenster;
import hashi.Insel;

public class SichereBruecke {
	
	private Fenster fenster;
	private ArrayList<Insel> inselliste;
	
	public SichereBruecke(Fenster f){
		fenster = f;
		inselliste = new ArrayList<Insel>();
		sucheInsel();
	}
	
	private void sucheInsel(){	
		for(int x = 0; x < fenster.getInseln().length; x++){
			for(int y = 0; y < fenster.getInseln()[x].length; y++){
				if ( fenster.getInseln()[x][y] != null && fenster.getInseln()[x][y].getAktuelleAnzahlVerbindung() != fenster.getInseln()[x][y].getMaxAnzahlVerbindung()){
					inselliste.add(fenster.getInseln()[x][y]);
				}
			}
		}
		
		if ( inselliste.size() == 0){
			return;
		} else {
			int idx = (int) (Math.random() * inselliste.size());
			konfliktfreieVerbindung(inselliste.get(idx));	
		}
	}
	
	private void konfliktfreieVerbindung(Insel insel){
		Nachbarn n = new Nachbarn(fenster);
		int nordY = n.getNordNachbar(insel.posx,insel.posy);
		int ostX = n.getOstNachbar(insel.posx, insel.posy);
		int suedY = n.getNordNachbar(insel.posx,insel.posy);
		int westX = n.getOstNachbar(insel.posx, insel.posy);
		
		if ( nordY != insel.posy){
			Insel nord = fenster.getInseln()[insel.posx][n.getNordNachbar(insel.posx, insel.posy)];
			if ( nord.anzahlVerbindungSued < fenster.getLoesung()[insel.posx][n.getNordNachbar(insel.posx, insel.posy)].anzahlVerbindungSued){
				fenster.getSpielfeld().verbindungHinzufuegen(insel, nord);
				return;
			}
		}
		
		if ( ostX != insel.posx){
			Insel ost = fenster.getInseln()[n.getOstNachbar(insel.posx, insel.posy)][insel.posy];
			if ( ost.anzahlVerbindungWest < fenster.getLoesung()[n.getOstNachbar(insel.posx, insel.posy)][insel.posy].anzahlVerbindungWest){
				fenster.getSpielfeld().verbindungHinzufuegen(insel, ost);
				return;
			}
		}
		
		if ( suedY != insel.posy){
			Insel sued = fenster.getInseln()[insel.posx][n.getSuedNachbar(insel.posx, insel.posy)];
			if ( sued.anzahlVerbindungNord < fenster.getLoesung()[insel.posx][n.getSuedNachbar(insel.posx, insel.posy)].anzahlVerbindungNord){
				fenster.getSpielfeld().verbindungHinzufuegen(insel, sued);
				return;
			}
		}
		
		if ( westX != insel.posx){
			Insel west = fenster.getInseln()[n.getWestNachbar(insel.posx, insel.posy)][insel.posy];
			if ( west.anzahlVerbindungOst < fenster.getLoesung()[n.getWestNachbar(insel.posx, insel.posy)][insel.posy].anzahlVerbindungOst){
				fenster.getSpielfeld().verbindungHinzufuegen(insel, west);
				return;
			}
		}	
	}

}
