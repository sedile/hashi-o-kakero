package verfahren;

public class Verbindung {
	
	public static final int NORD = 2;
	public static final int OST = 1;
	public static final int SUED = 3;
	public static final int WEST = 0;
	public static final int ERROR = -1;
	
	private boolean nachOben = false;
	private boolean nachRechts = false;
	private boolean nachUnten = false;
	private boolean nachLinks = false;
	
	private final int MAXRICHTUNGEN = 4;
	private int zufall;
	private int richtung;
	
	public Verbindung() {
		this.zufall = (int) (Math.random() * MAXRICHTUNGEN);
		this.richtung = 0;
	}
	
	/**
	 * Bestimmt zufaellig eine Richtung wo ein moeglicher Nachbar
	 * gefunden werden kann
	 * 
	 * @return die Richtung
	 */
	public int zufallsRichtung(){
		
		for(int i = 0; i < MAXRICHTUNGEN; i++){
			richtung = ( zufall + i) % MAXRICHTUNGEN;
			
			if ( richtung == NORD && !nachOben){
				nachOben = true;
				return NORD;
			} else if ( richtung == OST && !nachRechts){
				nachRechts = true;
				return OST;
			} else if ( richtung == SUED && !nachUnten){
				nachUnten = true;
				return SUED;
			} else if ( richtung == WEST && !nachLinks){
				nachLinks = true;
				return WEST;
			}
			}
		return ERROR;
		}
	}
