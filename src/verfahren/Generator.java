package verfahren;

import hashi.Insel;

import java.util.LinkedList;
import java.util.HashMap;
import java.awt.Point;
import java.io.PrintWriter;
import java.io.StringWriter;

public class Generator {

    private int breite;			// Spielfeldbreite
    private int hoehe;			// Spielfeldhoehe
    private Insel[][] loesung, ungeloest;
    private LinkedList<Insel> tempInseln;
    private HashMap<Insel, Point> inselKoordMap;
    
    public Generator(int breite, int hoehe){
        this.breite  = breite;
        this.hoehe = hoehe;
        this.loesung = new Insel[breite][hoehe];
        inselKoordMap = new HashMap<Insel, Point>();
        generiere(inselKoordMap);
    }

    /** Diese Methode Ueberprueft, ob direkte Nachbarn zu gegebenen Koordinaten exisitieren
     * 
     * @param x Die x-Koordinate
     * @param y Die y-Koordinate
     * @return true = es gibt keine benachbarte Insel
     *         false = es gibt eine benachbarte Insel (Konflikt)
     */
    private boolean keineNachbarInsel(int x, int y)
    {
        if((x > 0 && loesung[x - 1][y] != null) ||
           (x < breite - 1 && loesung[x + 1][y] != null) ||
           (y > 0 && loesung[x][y - 1] != null) ||
           (y < hoehe - 1 && loesung[x][y + 1] != null))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    /**
     * Generiert ein gueltiges Hashiwokakero-Raetsel
     * @param lInseln 
     */
	private void generiere(HashMap<Insel, Point> inselMap){
		inselKoordMap = inselMap;
		
		try{    	
    	// Vermerk, ob ein Feld mit einer Insel besetzt ist 
        boolean[][] istFeldBesetzt = new boolean[breite][hoehe];
        
        /* Eine Insel mit einen zufaelligen Punkt erzeugen und diese Insel mit
           0 initialisieren (maximale Anzahl von Verbindungen, restliche Verbindungen) */
        Point neueInselKoord = new Point((int)(Math.random() * this.breite), (int)(Math.random() * this.hoehe));
        
        Insel neueInsel = new Insel(neueInselKoord.x, neueInselKoord.y, 0);
        
        // Diese (und spaeter weitere) Insel in das Loesungsarray aufnehmen
        loesung[neueInselKoord.x][neueInselKoord.y] = neueInsel;

        
        /* Die Insel die erzeugt wurde, besetzt nun das Feld mit der jeweiligen
           Inselkoordinate */
        istFeldBesetzt[neueInselKoord.x][neueInselKoord.y] = true;
        
        /* Diese Liste enthaelt alle vorruebergehend erzeugten Inseln, wobei jede
         * Himmelsrichtung noch getestet wird, ob eine Verbindung zu anderen Insel
         * moeglich ist. Sollte eine Insel ausserhalb der Spielfeldgrenzen liegen oder direkte
         * Nachbarn exisitieren so wird die jewilige Insel aus der Liste entfernt */
        tempInseln = new LinkedList<Insel>();
        tempInseln.add(neueInsel);

        /* Diese Hashmap enthaelt als Schluesselwert eine Insel und als Zuweisung dessen
         * Inselkoordinate als (x,y) */
        inselKoordMap.put(neueInsel, neueInselKoord);
        
        // Erzeugt eine Hashmap von allen Inseln und unversuchten Richtungen
		/* Diese Hashmap enthaelt auch als Schluesselwert eine Insel und als Zuweisung
		 * alle Richtungen (NORD,OST,SUED,WEST,ERROR) die von dieser Insel ausgehen */
        HashMap<Insel, Verbindung> tempInselRichtungMap = new HashMap<Insel, Verbindung>();
        tempInselRichtungMap.put(neueInsel, new Verbindung());
        
        /* Die while-Schleife endet erst wenn die Inselliste leer ist. Dies ist der Fall
         * wenn entweder eine Anzahl von Inseln festgelegt wird oder es werden eine
         * zufï¿½llige Anzahl von Inseln erzeugt */
        while(!tempInseln.isEmpty())
        {
          // Waehle einen zufaelligen Index aus der Inselliste
          int zufallInselIndex = (int)(Math.random() * tempInseln.size());
            
          // aktuelleInsel ist nun die Insel aus den zufaellig ausgewaehlten Index
          Insel aktuelleInsel  = tempInseln.get(zufallInselIndex);

          	// Es wird eine zufaellige Richtung ausgesucht
            int richtung = tempInselRichtungMap.get(aktuelleInsel).zufallsRichtung();
            
            /* Der diffX und/oder diffY Wert wird addiert, um zu pruefen, ob man
             * zwei Felder in diese Richtung gehen kann, da keine direkten Nachbarn
             * zugelassen sind, sonst wird die aktuelle Insel samt Index aus der
             * Liste entfernt. */           
            int diffX;
            int diffY;

            switch(richtung)
            {
                case Verbindung.WEST:  	diffX = -1;  diffY =  0;  break;
                case Verbindung.OST: 	diffX =  1;  diffY =  0;  break;
                case Verbindung.NORD:   diffX =  0;  diffY = -1;  break;
                case Verbindung.SUED: 	diffX =  0;  diffY =  1;  break;
                default:
                    tempInseln.remove(zufallInselIndex);
                    continue;
            }
            
            /* Hier wird die Inselkoordinate erstellt undmit diffX oder diffY 
             * addiert je nach Richtung */
            Point aktuelleInselKoord = inselKoordMap.get(aktuelleInsel);
            int x = aktuelleInselKoord.x + 2*diffX;
            int y = aktuelleInselKoord.y + 2*diffY;  

            /* ueberpruefe die Arraygrenzen (x,y), pruefe weiterhin ob das direkte Nachbarfeld frei 
             * ist und ob das aktuelle Feldelement frei ist */
            if(x < 0 ||	y < 0 || x >= breite ||	 y >= hoehe ||
               istFeldBesetzt[aktuelleInselKoord.x + diffX][aktuelleInselKoord.y + diffY] || // Out
               istFeldBesetzt[x][y] ||
               !keineNachbarInsel(x, y))
            {
                continue;
            }
            
            /* Entstehen keine Konflikte, so wurde eine neue Inselkoordinate gefunden */
            neueInselKoord = new Point(x, y);

            while(x >= 0 && y >= 0 && x < breite && y < hoehe && !istFeldBesetzt[x][y] && Math.random() > 0.2)
            {
                if(keineNachbarInsel(x, y))
                {
                	/* moegliche neue Koordinate fuer eine neue Insel */
                    neueInselKoord.x = x;
                    neueInselKoord.y = y;
                }
                
                /* Nochmals diffX bzw. diffY auf die Inselkoordinate addieren */
                x += diffX;
                y += diffY;
            }

            /* Zufallswert von Bruecken zwischen der aktuellen Insel und der neuen
             * Insel. Kann nur Werte zwischen 1 und 2 annehmen */
            int anzahlVerbindungZwischenInsel = (int)(Math.random() * 2) + 1;
        	
        	/* Hier wird eine neue Insel samt der Anzahl der Bruecken fuer diese 
        	 * Insel erzeugt. Anschliessend wird diese Insel zu den Loesungsarray
        	 * hinzugefuegt und die maximale Anzahl an Verbindungen fuer diese Insel
        	 * gespeichert (Der Wert fuer die Insel) */
            neueInsel = new Insel(neueInselKoord.x, neueInselKoord.y, anzahlVerbindungZwischenInsel);
            loesung[neueInselKoord.x][neueInselKoord.y] = neueInsel;
            aktuelleInsel.maxAnzahlVerbindungen += anzahlVerbindungZwischenInsel; 

        	/* Markiere alle Felder zwischen diesen beiden Inseln als besetzt [???] */
            for(int i = aktuelleInselKoord.x, j = aktuelleInselKoord.y; i != neueInselKoord.x + diffX || j != neueInselKoord.y + diffY; i += diffX, j += diffY)
            {
                istFeldBesetzt[i][j] = true;
            }
            
            /* Hier wird angegeben, wieviele Verbindungen von der aktuellen Insel und der
             * neuen Insel (Nachbarinsel) exisitieren. Dies gilt fuer alle Himmelsrichtungen
             * falls existent (Bruecken hinzufuegen). */
            if(richtung == Verbindung.WEST)
            {
                aktuelleInsel.anzahlVerbindungWest = anzahlVerbindungZwischenInsel;
                neueInsel.anzahlVerbindungOst     = anzahlVerbindungZwischenInsel;
            }
            else if(richtung == Verbindung.OST)
            {
                aktuelleInsel.anzahlVerbindungOst = anzahlVerbindungZwischenInsel;
                neueInsel.anzahlVerbindungWest     = anzahlVerbindungZwischenInsel;
            }
            else if(richtung == Verbindung.NORD)
            {
                aktuelleInsel.anzahlVerbindungNord = anzahlVerbindungZwischenInsel;
                neueInsel.anzahlVerbindungSued     = anzahlVerbindungZwischenInsel;
            }
            else if(richtung == Verbindung.SUED)
            {
                aktuelleInsel.anzahlVerbindungSued = anzahlVerbindungZwischenInsel;
                neueInsel.anzahlVerbindungNord     = anzahlVerbindungZwischenInsel;
            }

            /* Die neue Insel (Nachbarinsel) wird in die Inselliste hinzugefuegt, 
             * damit diese genauso wie die aktuelle Insel geprueft werden kann, um so
             * mehr potentielle Inseln zu erhalten */
            tempInseln.add(neueInsel);
            inselKoordMap.put(neueInsel, neueInselKoord);

            tempInselRichtungMap.put(neueInsel, new Verbindung());
            
        } // while ende
        
        verbindungenLoeschen();
        
		}catch(Exception e){
			StringWriter stringWritter = new StringWriter();
			PrintWriter printWritter = new PrintWriter(stringWritter, true);
			e.printStackTrace(printWritter);
			printWritter.flush();
			stringWritter.flush(); 
			stringWritter.toString();
		}
	}
	
	private void verbindungenLoeschen(){
        ungeloest = new Insel[breite][hoehe];
        for(int i = 0; i < breite; i++){
            for(int j = 0; j < hoehe; j++){
                if(loesung[i][j] != null){
                    ungeloest[i][j] = new Insel(loesung[i][j].posx, loesung[i][j].posy,loesung[i][j].maxAnzahlVerbindungen);
                    ungeloest[i][j].anzahlVerbindungWest = 0;
                    ungeloest[i][j].anzahlVerbindungOst = 0;
                    ungeloest[i][j].anzahlVerbindungNord = 0;
                    ungeloest[i][j].anzahlVerbindungSued = 0;
                }
            }
        }  
	}
	
	public Insel[][] getInseln(){
		return ungeloest;
	}
	
	public Insel[][] getLoesung(){
		return loesung;
	}
	
}

