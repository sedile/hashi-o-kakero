package hashi;

public class Insel {

	public int posx;
	public int posy;	
	public int maxAnzahlVerbindungen;
	public int aktuelleVerbindungen; 			/* Wieviele Verbindungen von der Insel aktuell ausgehen */
	public int restVerbindungen; 				/* maxAnzahlVerbindungen - aktuelleVerbindungen */
	
	/* Anzahl der ausgehenden Verbindungen einer Insel */
	public int anzahlVerbindungNord = 0;
	public int anzahlVerbindungOst = 0;
	public int anzahlVerbindungSued = 0;
	public int anzahlVerbindungWest = 0;

	public Insel(int x, int y, int maxAnzahlVerbindungen){
		posx = x;
		posy = y;
		this.maxAnzahlVerbindungen = maxAnzahlVerbindungen;
	}
	
	public void setVerbindungNord(int nord){
		this.anzahlVerbindungNord = nord;
	}
	
	public void setVerbindungOst(int ost){
		this.anzahlVerbindungOst = ost;
	}
	
	public void setVerbindungSued(int sued){
		this.anzahlVerbindungSued = sued;
	}
	
	public void setVerbindungWest(int west){
		this.anzahlVerbindungWest = west;
	}
	
	public int getVerbindungNord(){
		return anzahlVerbindungNord;
	}
	
	public int getVerbindungOst(){
		return anzahlVerbindungOst;
	}
	
	public int getVerbindungSued(){
		return anzahlVerbindungSued;
	}
	
	public int getVerbindungWest(){
		return anzahlVerbindungWest;
	}

	public int getRestVerbindungen(){
		return maxAnzahlVerbindungen - aktuelleVerbindungen;
	}
	
	public int getMaxAnzahlVerbindung(){
		return maxAnzahlVerbindungen;
	}
	
	public int getAktuelleAnzahlVerbindung(){
		return anzahlVerbindungNord + anzahlVerbindungOst + anzahlVerbindungSued + anzahlVerbindungWest;
	}
	
}

